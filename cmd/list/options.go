package list

type CommandOptions struct {
	Verbose bool
	Tag     bool
	Test    bool
}
