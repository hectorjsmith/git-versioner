package latest

type CommandOptions struct {
	Verbose bool
	Tag     bool
}
