package checkout

type CommandOptions struct {
	Version string
	Latest  bool
}
